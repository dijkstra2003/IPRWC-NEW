package webshop.service;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;
import webshop.model.User;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import java.util.Optional;


/**
 * Created by Luc Dijkstra on 15-Jan-18.
 */
public class UserAuthenticator implements Authenticator<BasicCredentials, User>{

    private final UserService service;

    @Inject
    public UserAuthenticator(UserService userService)
    {
        this.service = userService;
    }


    @Override
    public Optional<User> authenticate(BasicCredentials credentials) throws AuthenticationException {

        User user = service.loginUser(credentials.getUsername(), credentials.getPassword());
        if(user.getEmail() != null && user.getEmail().equals(credentials.getUsername())){
            return Optional.of(user);
        }else
        {
            throw new WebApplicationException(403);
        }

    }
}

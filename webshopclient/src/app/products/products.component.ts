import { Component, OnInit } from '@angular/core';
import {Product} from "./Product";
import {ProductService} from "./product.service";
import {Cart} from "../cart/Cart";
import {Observable} from "rxjs/Observable";
import {isUndefined} from "util";
import {forEach} from "@angular/router/src/utils/collection";
import {MenuComponent} from "../shared/menu/menu.component";
import {CartService} from "../cart/cart.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [MenuComponent]
})
export class ProductsComponent implements OnInit {

  allProducts: Product[];
  userid: number;
  carts: Array<Cart> = [];
  cartObs: Observable<Array<Cart[]>>;

  constructor(private productService: ProductService, private menuComponent: MenuComponent, private cartService: CartService) {
   this.productService.getAllProducts().subscribe(
      products => {
      this.allProducts = products;
    });

    let id = localStorage.getItem('userid');
    this.userid = +id;
  }

  ngOnInit() {
  }

  public addToCart(product: Product){
    if(this.userid == 0){
      alert("U moet ingelogd zijn om een bestelling in uw winkelwagen te doen");
    } else {
      if(isUndefined(this.carts[0])) {
        var tempCart: Cart = new Cart(1, this.userid, product.id);
        this.carts.push(tempCart);
      } else {
        var check = false;
        for(let cart in this.carts){
          if(this.carts[cart].productId == product.id){
            var tempCart: Cart = new Cart(1, this.userid, product.id);
            this.carts[cart].amount += 1;
            check = false;
            break;
          } else {
            check = true;
          }
        }
        if(check){
          var tempCart: Cart = new Cart(1, this.userid, product.id);
          this.carts.push(tempCart);
        }
      }
    }
    this.cartService.addCart(this.carts);
  }
}


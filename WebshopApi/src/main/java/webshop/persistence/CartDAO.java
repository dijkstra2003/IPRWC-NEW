package webshop.persistence;

import webshop.model.Cart;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Luc Dijkstra on 15-Jan-18.
 */
public class CartDAO {
    private PreparedStatement getCartsByUserId;
    private PreparedStatement addProductToCart;
    private PreparedStatement updateCart;
    private PreparedStatement deleteCart;

    private final List<Cart> filledCart;
    private Connection dbConnection;
    private Database database = Database.getDatabase();

    public CartDAO() throws IOException, SQLException {
        filledCart = new ArrayList<>();
        dbConnection = database.getDbConnection();
        preparedStatements();
    }

    public Collection<Cart> getCartById(int user){
        try{
            filledCart.clear();
            getCartsByUserId.setInt(1, user);
            ResultSet carts = getCartsByUserId.executeQuery();
            while(carts.next()) {
                int amount = carts.getInt("winkelwagen_hoeveelheid");
                int userid = carts.getInt("winkelwagen_gebruiker_id");
                int productid = carts.getInt("winkelwagen_product_id");

                Cart cart = new Cart(amount, userid, productid);
                filledCart.add(cart);
            }
            return filledCart;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addProductToCart(Cart[] cart){
        delete(cart[0].getUserId());
        for(Cart c: cart) {
            try {
                addProductToCart.setInt(1, c.getAmount());
                addProductToCart.setInt(2, c.getUserId());
                addProductToCart.setInt(3, c.getProductId());
                addProductToCart.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void update(Cart cart){
        try {
            updateCart.setInt(1, cart.getAmount());
            updateCart.setInt(3, cart.getProductId());
            updateCart.setInt(2, cart.getUserId());
            updateCart.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int userid){
        try {
            deleteCart.setInt(1, userid);
            deleteCart.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private void preparedStatements(){
        try {
            getCartsByUserId = dbConnection.prepareStatement("SELECT * FROM winkelwagen WHERE winkelwagen_gebruiker_id = ?");
            addProductToCart = dbConnection.prepareStatement("INSERT INTO winkelwagen(winkelwagen_hoeveelheid, winkelwagen_gebruiker_id, winkelwagen_product_id) VALUES(?,?,?);");
            updateCart = dbConnection.prepareStatement("UPDATE winkelwagen SET winkelwagen_hoeveelheid = ? WHERE winkelwagen_gebruiker_id = ? AND winkelwagen_product_id = ?;");
            deleteCart = dbConnection.prepareStatement("DELETE FROM winkelwagen WHERE winkelwagen_gebruiker_id = ?;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

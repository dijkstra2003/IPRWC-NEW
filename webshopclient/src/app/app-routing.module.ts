import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {ProductsComponent} from "./products/products.component";
import {CartComponent} from "./cart/cart.component";
import {LoginComponent} from "./user/login/login.component";
import {AdminComponent} from "./admin/admin.component";
import {UserComponent} from "./user/user.component";

/**
 * Created by Luc Dijkstra on 14-Jan-18.
 */

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full'},
  { path: 'producten', component: ProductsComponent},
  { path: 'winkelwagen', component: CartComponent},
  { path: 'login', component: LoginComponent},
  { path: 'admin', component: AdminComponent},
  { path: 'account', component: UserComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule]
})

export class AppRoutingModule {

}


package webshop.persistence;

import webshop.model.Product;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This is the DAO to execute SQL-Queries
 * for the Products in the database.
 *
 * Created by Luc Dijkstra on 13-Jan-18.
 */
public class ProductDAO {
    private PreparedStatement getProducts;
    private PreparedStatement addProduct;
    private PreparedStatement deleteProduct;
    private PreparedStatement updateProduct;
    private PreparedStatement getProductById;

    private final List<Product> allProducts;
    private Product product;
    private Connection dbConnection;
    private Database database = Database.getDatabase();

    public ProductDAO() throws IOException, SQLException {
        allProducts = new ArrayList<>();
        product = new Product();
        dbConnection = database.getDbConnection();
        preparedStatements();
    }

    public synchronized Product getProductById(int productid){
        try {
            getProductById.setInt(1, productid);
            ResultSet rs = getProductById.executeQuery();
            while(rs.next()){
                int id = productid;
                String title = rs.getString("product_titel");
                String description = rs.getString("product_omschrijving");
                float price = rs.getFloat("product_prijs");
                String picture = rs.getString("product_afbeelding");

                Product product = new Product(id, title, description, price, picture);
                this.product = product;
            }
            return product;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }
    public Collection<Product> getProducts(){
        try {
            allProducts.clear();
            ResultSet products = getProducts.executeQuery();
            while(products.next()) {
                int id = products.getInt("product_id");
                String title = products.getString("product_titel");
                String description = products.getString("product_omschrijving");
                float price = products.getFloat("product_prijs");
                String picture = products.getString("product_afbeelding");

                Product product = new Product(id, title, description, price, picture);
                allProducts.add(product);
            }
            return allProducts;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void add(Product product) {
        try {
            addProduct.setString(1, product.getTitle());
            addProduct.setString(2, product.getDescription());
            addProduct.setFloat(3, product.getPrice());
            addProduct.setString(4, product.getPicture());
            addProduct.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(Product product) {
        try {
            updateProduct.setString(1, product.getTitle());
            updateProduct.setString(2, product.getDescription());
            updateProduct.setFloat(3, product.getPrice());
            updateProduct.setString(4, product.getPicture());
            updateProduct.setInt(5, product.getId());
            updateProduct.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int product) {
        System.out.println("DAO: " + product);
        try {
            deleteProduct.setInt(1, product);
            System.out.println(deleteProduct);
            deleteProduct.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void preparedStatements(){
        try {
            getProducts = dbConnection.prepareStatement("SELECT * FROM product;");
            addProduct = dbConnection.prepareStatement("INSERT INTO product(product_titel, product_omschrijving, product_prijs, product_afbeelding) VALUES(?,?,?,?);");
            updateProduct = dbConnection.prepareStatement("UPDATE product SET product_titel = ?, product_omschrijving = ?, product_prijs = ?, product_afbeelding = ? WHERE product_id =?;");
            deleteProduct = dbConnection.prepareStatement("DELETE FROM product WHERE product_id = ?;");
            getProductById = dbConnection.prepareStatement("SELECT * FROM product WHERE product_id = ?;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

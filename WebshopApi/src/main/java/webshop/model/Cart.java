package webshop.model;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import webshop.View;

/**
 * Created by Luc Dijkstra on 15-Jan-18.
 */
public class Cart{
    @NotEmpty
    @Length(min = 1, max = 11)
    @JsonView(View.Public.class)
    private int amount;

    @NotEmpty
    @Length(min = 1, max = 11)
    @JsonView(View.Public.class)
    private int userId;

    @NotEmpty
    @Length(min = 1, max = 11)
    @JsonView(View.Public.class)
    private int productId;

    public Cart(int amount, int userId, int productId) {
        this.amount = amount;
        this.userId = userId;
        this.productId = productId;
    }

    public Cart(int userId, int productId) {
        this.userId = userId;
        this.productId = productId;
    }

    public Cart(){

    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}

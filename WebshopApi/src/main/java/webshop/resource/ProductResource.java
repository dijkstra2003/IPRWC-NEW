package webshop.resource;

import com.fasterxml.jackson.annotation.JsonView;
import webshop.View;
import webshop.model.Product;
import webshop.service.ProductService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;


@Singleton
@Path("products")
@Produces(MediaType.APPLICATION_JSON)
public class ProductResource {

    private final ProductService service;

    @Inject
    public ProductResource(ProductService service){
        this.service = service;
    }

    @GET
    @JsonView(View.Public.class)
    public Collection<Product> retrieveAll(){
        return service.getAll();
    }

    @POST
    @RolesAllowed("admin")
    @JsonView(View.Protected.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void createProduct(Product product) {
        service.add(product);
    }

    @PUT
    @RolesAllowed("admin")
    @JsonView(View.Protected.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateProduct(Product product) {
        service.update(product);
    }

    @DELETE
    @RolesAllowed("admin")
    @Path("/{id}")
    @JsonView(View.Protected.class)
    public void deleteProduct(@PathParam("id") int id)
    {
        service.delete(id);
    }

    @GET
    @Path("/{id}")
    @JsonView(View.Protected.class)
    public Product getProductById(@PathParam("id") int id)
    {
        return service.getProductById(id);
    }
}

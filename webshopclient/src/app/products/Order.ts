/**
 * Created by Luc Dijkstra on 17-Jan-18.
 */
export class Order{
  constructor(
    public title?: string,
    public amount?: number,
    public price?: number
  ){

  }
}

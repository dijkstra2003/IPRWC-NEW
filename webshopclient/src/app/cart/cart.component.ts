import { Component, OnInit } from '@angular/core';
import {Cart} from "./Cart";
import {CartService} from "./cart.service";
import {Order} from "../products/Order";
import {ProductService} from "../products/product.service";
import {Observable} from "rxjs/Observable";
import {ProductsComponent} from "../products/products.component";
import {Product} from "../products/Product";
import {MenuComponent} from "../shared/menu/menu.component";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  providers:[ProductsComponent, MenuComponent]
})
export class CartComponent implements OnInit {
  public carts: Observable<Cart[]> = new Observable;
  order: Order[] = [];
  allProducts: Product[] = [];
  totalPrice: number = 0;
  userid: number;

  constructor(private cartService: CartService, private productService: ProductService, private productComponent: ProductsComponent) {
    let id = localStorage.getItem('userid');
    this.userid = +id;
    this.carts = this.cartService.getCart(this.userid);
    this.productService.getAllProducts().subscribe(
      products => {
        this.allProducts = products;
        this.carts.forEach(
          cart => {
            cart.forEach( cartItem => {
                this.allProducts.find((p) => {
                    if(p.id === cartItem.productId){
                      this.productService.getProductById(p.id).subscribe(
                        data => {
                          var productTitle = data.title;
                          var productPrice = data.price;
                          var tempPrice: number = (cartItem.amount * productPrice);
                          tempPrice = parseFloat(tempPrice.toFixed(2));
                          var tempOrder: Order = new Order(productTitle, cartItem.amount, tempPrice);
                          this.order.push(tempOrder);
                          this.totalPrice += (cartItem.amount * productPrice);
                          this.totalPrice = parseFloat(this.totalPrice.toFixed(2))
                          }
                      );
                    }
                    return false;
                  }
                );
              }
            );
          }
        );
        console.log(this.order);
      });
  }

  ngOnInit() {
  }
}

package webshop.service;

import webshop.model.User;
import webshop.persistence.UserDAO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collection;

@Singleton
public class UserService extends BaseService{
    private final UserDAO dao;

    @Inject
    public UserService(UserDAO dao) {
        this.dao = dao;
    }

    public Collection<User> getAll() {
        return dao.getUsers();
    }

    public void add(User user) {
        this.dao.add(user);
    }

    public void update(User user) {
        this.dao.update(user);
    }

    public void delete(int id) {
        this.dao.delete(id);
    }

    public User loginUser(String userName, String password) {
        User user  = dao.loginUser(userName);
        Boolean correctPass = false;
        if (user.getEmail().equals(userName) && user.getPassword().equals(password)){
            correctPass = true;
        }

        if(correctPass){
            return user;
        } else {
            return new User();
        }
    }

    public User getUserById(String email){
        return this.dao.loginUser(email);
    }


}

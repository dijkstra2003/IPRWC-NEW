package webshop;

import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.forms.MultiPartBundle;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import webshop.model.User;
import webshop.persistence.CartDAO;
import webshop.persistence.ProductDAO;
import webshop.persistence.UserDAO;
import webshop.resource.CartResource;
import webshop.resource.ProductResource;
import webshop.resource.UserResource;
import webshop.service.*;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.io.IOException;
import java.sql.SQLException;
import java.util.EnumSet;

/**
 * Created by Luc Dijkstra on 13-Jan-18.
 */
public class ApiApplication extends Application<ApiConfiguration>{

    public static void main(String[] args) throws Exception {
        new ApiApplication().run(args);
    }

    @Override
    public String getName() {
        return "Webshop-API";
    }

    @Override
    public void initialize(Bootstrap<ApiConfiguration> bootstrap) {
        bootstrap.addBundle(new MultiPartBundle());
    }

    @Override
    public void run(ApiConfiguration configuration,
                    Environment environment) throws IOException, SQLException{

        UserService userService = new UserService(new UserDAO());
        UserResource userResource = new UserResource(userService);
        ProductService productService = new ProductService(new ProductDAO());
        ProductResource productResource = new ProductResource(productService);
        CartService cartService = new CartService(new CartDAO());
        CartResource cartResource = new CartResource(cartService);

        environment.jersey().register(userResource);
        environment.jersey().register(productResource);
        environment.jersey().register(cartResource);

        environment.jersey().register(new AuthDynamicFeature(new BasicCredentialAuthFilter.Builder<User>()
                .setAuthenticator(new UserAuthenticator(userService))
                .setAuthorizer(new UserAuthorizer())
                .setRealm("SECRET REALM")
                .buildAuthFilter()));
        environment.jersey().register(RolesAllowedDynamicFeature.class);
        environment.jersey().register(new AuthValueFactoryProvider.Binder<>(User.class));

        configureCors(environment);
    }

    private void configureCors(Environment environment) {
        FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        filter.setInitParameter("allowCredentials", "true");
    }
}

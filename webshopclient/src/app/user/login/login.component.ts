import { Component, OnInit } from '@angular/core';
import {User} from "../User";
import {UserService} from "../user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User = new User();
  remember;
  register: boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit() {

  }

  onSubmit() {
    this.userService.login(this.user, true)
  }

  registerMe(){
    this.userService.register(this.user);
  }
}


package webshop.resource;

import com.fasterxml.jackson.annotation.JsonView;
import webshop.View;
import webshop.model.Cart;
import webshop.service.CartService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Singleton
@Path("cart")
@Produces(MediaType.APPLICATION_JSON)
public class CartResource {

    private final CartService service;

    @Inject
    public CartResource(CartService service){
        this.service = service;
    }

    @GET
    @Path("{userid}")
    @JsonView(View.Public.class)
    public Collection<Cart> retrieveCart(@PathParam("userid") int userid){
        return service.getCart(userid);
    }

    @POST
    @JsonView(View.Protected.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void addProductToCart(Cart[] cart){
        service.add(cart);
    }

    @PUT
    @JsonView(View.Protected.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateCart(Cart cart){
        service.update(cart);
    }

    @DELETE
    @JsonView(View.Public.class)
    public void deleteCarts(int userid){
        service.delete(userid);
    }
}

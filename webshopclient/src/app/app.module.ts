import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MenuComponent } from './shared/menu/menu.component';
import { ProductsComponent } from './products/products.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { CartComponent } from './cart/cart.component';
import {LoginComponent} from "./user/login/login.component";
import { AdminComponent } from './admin/admin.component';
import {UserService} from "./user/user.service";
import {AuthorizationService} from "./shared/auth.service";
import {ProductService} from "./products/product.service";
import {ApiService} from "./shared/api.service";
import { FormsModule } from '@angular/forms';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {HttpModule} from "@angular/http";
import {Ng2SmartTableModule} from "ng2-smart-table";
import {CartService} from "./cart/cart.service";


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ProductsComponent,
    HomeComponent,
    UserComponent,
    CartComponent,
    LoginComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    Ng2SmartTableModule
  ],
  providers: [
    UserService, AuthorizationService, ProductService, ApiService, CartService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

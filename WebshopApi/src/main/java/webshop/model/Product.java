package webshop.model;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import webshop.View;

/**
 * This is the model for the Product entity.
 *
 * Created by Luc Dijkstra on 13-Jan-18.
 */
public class Product{
    @NotEmpty
    @Length(min = 1, max = 11)
    @JsonView(View.Public.class)
    private int id;

    @NotEmpty
    @Length(min = 1, max = 255)
    @JsonView(View.Public.class)
    private String title;

    @Length(min = 1, max = 1024)
    @JsonView(View.Public.class)
    private String description;

    @NotEmpty
    @JsonView(View.Public.class)
    private float price;

    @Length(min = 1, max = 255)
    @JsonView(View.Public.class)
    private String picture;

    public Product(){

    }

    public Product(String title, String description, float price, String picture){
        this.title = title;
        this.description = description;
        this.price = price;
        this.picture = picture;
    }

    public Product(int id, String title, String description, float price, String picture){
        this.id =  id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}

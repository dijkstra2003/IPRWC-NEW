/**
 * Created by Luc Dijkstra on 15-Jan-18.
 */
export class User {
  constructor(
    public id?: number,
    public email?: string,
    public password?: string,
    public firstname?: string,
    public preposition?: string,
    public lastname?: string,
    public role?: string
  )
  {

  }
}

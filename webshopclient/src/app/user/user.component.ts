import { Component, OnInit } from '@angular/core';
import {User} from "./User";
import {UserService} from "./user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public user: User = new User();
  currentUser: User = new User();
  constructor(private userService: UserService) {
    this.userService.getMe().subscribe(
      data => {
        this.currentUser = data;
        console.log(data)}
    );
  }

  ngOnInit() {
  }

  public update(){
    console.log(this.currentUser);
    this.userService.updateUser(this.currentUser);
  }

  public deleteAccount(){
    var userid = +localStorage.getItem('userid');
    this.userService.deleteUser(userid);
    localStorage.clear();
  }

}

package webshop.service;

import webshop.model.Cart;
import webshop.model.User;
import webshop.persistence.CartDAO;

import javax.inject.Inject;
import java.util.Collection;

/**
 * Created by Luc Dijkstra on 15-Jan-18.
 */
public class CartService {
    private final CartDAO dao;

    @Inject
    public CartService(CartDAO dao){
        this.dao = dao;
    }

    public Collection<Cart> getCart(int userid){
        return this.dao.getCartById(userid);
    }

    public void add(Cart[] cart){
        this.dao.addProductToCart(cart);
    }

    public void update(Cart cart) {
        this.dao.update(cart);
    }

    public void delete(int userid) {
        this.dao.delete(userid);
    }
}

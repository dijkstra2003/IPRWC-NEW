package webshop.persistence;

import webshop.model.User;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This is the DAO to execute SQL-Queries
 * for the Users in the database.
 *
 * Created by Luc Dijkstra on 13-Jan-18.
 */
public class UserDAO {
    private PreparedStatement getUsers;
    private PreparedStatement addUser;
    private PreparedStatement updateUser;
    private PreparedStatement deleteUser;
    private PreparedStatement getLoginUser;

    private final List<User> allUsers;
    private Connection dbConnection;
    private Database database = Database.getDatabase();

    public UserDAO() throws IOException, SQLException {
        allUsers = new ArrayList<>();
        dbConnection = database.getDbConnection();
        preparedStatements();
    }

    public Collection<User> getUsers(){
        try {
            allUsers.clear();
            ResultSet users = getUsers.executeQuery();
            while(users.next()) {
                int id = users.getInt("gebruiker_id");
                String email = users.getString("gebruiker_email");
                String password = users.getString("gebruiker_wachtwoord");
                String firstname = users.getString("gebruiker_voornaam");
                String preposition = users.getString("gebruiker_tussenvoegsel");
                String lastname = users.getString("gebruiker_achternaam");
                String role = users.getString("gebruiker_rol");
                User user = new User(id, email, password, firstname, preposition, lastname, role);
                allUsers.add(user);
            }
            return allUsers;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public User loginUser(String userName) {
        User loginUser = new User();
        try {
            getLoginUser.setString(1, userName);
            ResultSet resultSet = getLoginUser.executeQuery();
            if(resultSet.next()){
                loginUser.setId(resultSet.getInt("gebruiker_id"));
                loginUser.setEmail( resultSet.getString("gebruiker_email"));
                loginUser.setPassword( resultSet.getString("gebruiker_wachtwoord"));
                loginUser.setFirstname(resultSet.getString("gebruiker_voornaam"));
                loginUser.setPreposition(resultSet.getString("gebruiker_tussenvoegsel"));
                loginUser.setLastname(resultSet.getString("gebruiker_achternaam"));
                loginUser.setRole(resultSet.getString("gebruiker_rol"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loginUser;
    }

    public void add(User user){
        try {
            addUser.setString(1, user.getEmail());
            addUser.setString(2, user.getPassword());
            addUser.setString(3, user.getFirstname());
            addUser.setString(4, user.getPreposition());
            addUser.setString(5, user.getLastname());
            addUser.setString(6, "client");
            addUser.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        try {
            deleteUser.setInt(1, id);
            deleteUser.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(User user){
        try {
            updateUser.setString(1, user.getPassword());
            updateUser.setString(2, user.getFirstname());
            updateUser.setString(3, user.getPreposition());
            updateUser.setString(4, user.getLastname());
            updateUser.setString(5, user.getEmail());
            updateUser.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void preparedStatements(){
        try{
            getUsers = dbConnection.prepareStatement("SELECT * FROM gebruiker;");
            addUser = dbConnection.prepareStatement("INSERT INTO gebruiker(gebruiker_email, gebruiker_wachtwoord, gebruiker_voornaam, gebruiker_tussenvoegsel, gebruiker_achternaam, gebruiker_rol) VALUES(?,?,?,?,?,?);");
            updateUser = dbConnection.prepareStatement("UPDATE gebruiker SET gebruiker_wachtwoord = ?, gebruiker_voornaam = ?, gebruiker_tussenvoegsel = ?, gebruiker_achternaam = ? WHERE gebruiker_email = ?;");
            deleteUser = dbConnection.prepareStatement("DELETE FROM gebruiker WHERE gebruiker_id = ?;");
            getLoginUser = dbConnection.prepareStatement("SELECT * FROM gebruiker WHERE gebruiker_email = ?;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package webshop.service;

import io.dropwizard.auth.Authorizer;
import webshop.model.User;

/**
 * Created by Luc Dijkstra on 15-Jan-18.
 */
public class UserAuthorizer implements Authorizer<User> {

    @Override
    public boolean authorize(User user, String role){
        return user.getRole().equals(role);
    }
}

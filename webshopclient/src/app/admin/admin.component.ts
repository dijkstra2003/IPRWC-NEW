import { Component, OnInit } from '@angular/core';
import {Product} from "../products/Product";
import {Observable} from "rxjs/Observable";
import {ProductService} from "../products/product.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  allProducts: Observable<Product[]>;
  product: Product = new Product();

  settings = {
    add: {confirmCreate: true},
    edit: {confirmSave: true},
    delete: {confirmDelete: true},
    pager : {
      display: true,
      perPage: 20
    },
    columns: {
      title: {
        title: 'Titel'
      },
      description: {
        title: 'Beschrijving'
      },
      price: {
        title: 'Prijs'
      },
      picture: {
        title: 'Afbeelding'
      },
    }

  };

  constructor(private productService: ProductService) {
    this.allProducts = this.productService.getAllProducts();
  }

  ngOnInit() {
  }

  addProduct(event){
    this.product = event.newData;
    if (window.confirm(("Weet u zeker dat u dit product wilt toevoegen?"))){
      event.confirm.resolve();
      this.productService.addProduct(this.product);
    } else {
      event.confirm.reject();
      window.alert("Product is niet toegevoegd");
    }
  }

  updateProduct(event){
    if(window.confirm(("Weet u zeker dat u het product wilt aanpassen?"))){
      event.confirm.resolve();
      this.product = event.newData;
      this.productService.editProduct(this.product);
    } else {
      event.confirm.reject();
      window.alert("Product is niet aangepast.");
    }
  }

  deleteProduct(event){
    if (window.confirm(('Weet je zeker dat je dit product wilt verwijderen?'))) {
      event.confirm.resolve();
      this.product = event.data;
      this.productService.deleteProduct(this.product);
    } else {
      event.confirm.reject();
      window.alert('Product is niet verwijderd.');
    }
  }

}

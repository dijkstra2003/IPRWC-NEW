package webshop;

/**
 * Created by Luc Dijkstra on 13-Jan-18.
 */

public class View
{
    public static class Internal extends Private {}

    public static class Private extends Protected {}

    public static class Protected extends Public {}

    public static class Public {}
}


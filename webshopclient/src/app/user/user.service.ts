/**
 * Created by Luc Dijkstra on 15-Jan-18.
 */
import {Injectable} from "@angular/core";
import {ApiService} from "../shared/api.service";
import {Router} from "@angular/router";
import {AuthorizationService} from "../shared/auth.service";
import {User} from "./User";

@Injectable()
export class UserService {

  userid: number;
  private user: User = new User();
  constructor(private api: ApiService, private authService: AuthorizationService, private router: Router) {

  }

  public login(user: User, remember: boolean): void
  {
    this.authService.setAuthorization(user.email, user.password);

    this.api.get<User>('users/me').subscribe(
      data => {
        this.authService.storeAuthorization(data, remember);
        this.setLoginSession(data, remember);
        this.goHome();
      },

      error =>
      {
        alert('Het inloggen is mislukt');
      }
    );
  }

  public register(user: User): void
  {
    let data =
      {
        email: user.email,
        password: user.password,
        firstname: user.firstname,
        preposition: user.preposition,
        lastname: user.lastname,
        role: user.role
      };

    this.api.post<void>('users', data).subscribe
    (
      data =>
      {
        this.goHome();
      },
      error =>
      {
        alert('Het registreren is mislukt');
      }
    );
  }

  public logout(){
    this.goHome();
  }

  private goHome(){
    this.router.navigate(['']);
  }

  public getMe() {
    return this.api.get<User>('users/' + localStorage.getItem('email'));
  }

  public setLoginSession(User: User, local: boolean): void{
    let storage = local ? localStorage : sessionStorage;

    storage.setItem('userid', User.id.toString());
    storage.setItem('email', User.email);

  }

  public updateUser(user: User): void {

    let data = {
      email: user.email,
      password: user.password,
      firstname: user.firstname,
      preposition: user.preposition,
      lastname: user.lastname,
      role: user.role
    };

    this.api.put('users', data).subscribe(
      data => {
        alert('Het aanpassen is gelukt');
      },
      error => {
        alert('Er is iets mis gegaan bij het aanpassen');
      }
    );
  }

  public deleteUser(userid: number): void {
    this.api.delete<number>('users/' + userid).subscribe(
      data => {
        location.reload();
        this.router.navigate(['']);
      },
      error => {
        alert("Het verwijderen van het account is niet gelukt.")
      }
    )
  }
}

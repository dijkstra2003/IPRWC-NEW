import { Component, OnInit } from '@angular/core';
import {AuthorizationService} from "../auth.service";
import {ProductsComponent} from "../../products/products.component";
import {ProductService} from "../../products/product.service";
import {CartService} from "../../cart/cart.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit {

  loggedIn: boolean = false;
  cartSize: number = 0;

  constructor(private authService: AuthorizationService, private productService: ProductService) {
    this.loggedIn = this.authService.hasAuthorization();
    authService.authorized$.subscribe(
      authorized =>
      {
        this.loggedIn = authorized;
      }
    );
  }

  ngOnInit() {
  }
}

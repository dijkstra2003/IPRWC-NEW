package webshop.resource;

import com.fasterxml.jackson.annotation.JsonView;
import io.dropwizard.auth.Auth;
import webshop.View;
import webshop.model.User;
import webshop.service.UserService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

import static org.glassfish.jersey.server.model.Parameter.Source.PATH;


@Singleton
@Path("users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private final UserService service;

    @Inject
    public UserResource(UserService service){
        this.service = service;
    }

    @GET
    @RolesAllowed("admin")
    @JsonView(View.Public.class)
    public Collection<User> retrieveAll(){
        return service.getAll();
    }

    @POST
    @JsonView(View.Protected.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void createUser(User user) {
        service.add(user);
    }

    @PUT
    @JsonView(View.Protected.class)
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateUser(User user) {
        service.update(user);
    }

    @DELETE
    @Path("/{id}")
    @JsonView(View.Protected.class)
    public void deleteUser(@PathParam("id") int id) {
        service.delete(id);
    }

    @GET
    @Path("me")
    @JsonView(View.Public.class)
    public User authenticate(@Auth User authenticator)
    {
        return authenticator;
    }

    @GET
    @Path("{email}")
    @JsonView(View.Public.class)
    public User getLoggedUser(@PathParam("email") String email){
        return service.getUserById(email);
    }
}

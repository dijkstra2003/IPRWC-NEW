package webshop.model;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import webshop.View;

import java.security.Principal;

/**
 * This is the model for the User entity.
 *
 * Created by Luc Dijkstra on 13-Jan-18.
 */
public class User implements Principal {
    @NotEmpty
    @Length(min = 1, max = 11)
    @JsonView(View.Public.class)
    private int id;

    @NotEmpty
    @Length(min = 3, max = 128)
    @JsonView(View.Public.class)
    private String email;

    @NotEmpty
    @Length(min = 5, max = 255)
    @JsonView(View.Public.class)
    private String password;

    @NotEmpty
    @Length(min = 1, max = 255)
    @JsonView(View.Public.class)
    private String firstname;

    @Length(min = 1, max = 50)
    @JsonView(View.Public.class)
    private String preposition;

    @NotEmpty
    @Length(min = 1, max = 255)
    @JsonView(View.Public.class)
    private String lastname;

    @NotEmpty
    @Length(min = 1, max = 255)
    @JsonView(View.Public.class)
    private String role;

    public User(){

    }

    public User(String email, String password, String firstname, String preposition, String lastname, String role) {
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.preposition = preposition;
        this.lastname = lastname;
        this.role = role;
    }

    public User(int id, String email, String password, String firstname, String preposition, String lastname, String role) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.preposition = preposition;
        this.lastname = lastname;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPreposition() {
        return preposition;
    }

    public void setPreposition(String preposition) {
        this.preposition = preposition;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String getName() {
        return null;
    }
}

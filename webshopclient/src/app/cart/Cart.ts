/**
 * Created by Luc Dijkstra on 16-Jan-18.
 */
export class Cart
{
  constructor(
    public amount?: number,
    public userId?: number,
    public productId?: number
  ){

  }
}

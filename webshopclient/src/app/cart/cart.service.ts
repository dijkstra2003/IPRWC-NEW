/**
 * Created by Luc Dijkstra on 16-Jan-18.
 */
import {Injectable} from "@angular/core";
import {ApiService} from "../shared/api.service";
import {Cart} from "./Cart";
import {Observable} from "rxjs/Observable";

@Injectable()
export class CartService{
  public carts: Observable<Cart[]> = new Observable();

  constructor(private api: ApiService){

  }


  public setCart(carts: Observable<Cart[]>){
    this.carts = carts;
  }

  public addCart(cart: Cart[]): void{

    this.api.post<Cart[]>('cart', cart).subscribe();
  }

  public getCart(id: number): Observable<Cart[]>{
    return this.carts = this.api.get('cart/' + id);
  }

}

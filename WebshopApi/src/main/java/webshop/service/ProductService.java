package webshop.service;

import webshop.model.Product;
import webshop.persistence.ProductDAO;

import javax.inject.Inject;
import java.util.Collection;

/**
 * Created by Luc Dijkstra on 13-Jan-18.
 */
public class ProductService {
    private final ProductDAO dao;

    @Inject
    public ProductService(ProductDAO dao) {
        this.dao = dao;
    }

    public Collection<Product> getAll() {
        return dao.getProducts();
    }

    public Product getProductById(int id){
        return this.dao.getProductById(id);
    }

    public void add(Product product) {
        this.dao.add(product);
    }

    public void update(Product product) {
        this.dao.update(product);
    }

    public void delete(int product) {
        this.dao.delete(product);
    }
}

/**
 * Created by Luc Dijkstra on 14-Jan-18.
 */
export class Product
{
  constructor(
    public id?: number,
    public title?: string,
    public description?: string,
    public price?: number,
    public picture?: string
  ){

  }
}

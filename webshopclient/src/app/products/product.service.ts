/**
 * Created by Luc Dijkstra on 14-Jan-18.
 */
import {Injectable} from "@angular/core";
import {Product} from "./Product";
import {ApiService} from "../shared/api.service";
import {Observable} from "rxjs/Observable";
import {Cart} from "../cart/Cart";

@Injectable()
export class ProductService {
  productArray: Product[];
  carts: Array<Cart> = [];

  constructor(private api: ApiService){}

  public getAllProducts(): Observable<Product[]>{
    return this.api.get<Product[]>('products')
  }

  public addProduct(product: Product): void {
    let data =
      {
        title: product.title,
        description: product.description,
        price: product.price,
        picture: product.picture
      };

    this.api.post('/products', data).subscribe(

      data => {
        alert("Het toevoegen van een product is gelukt.");
      },
      error => {
        alert("Het toevoegen van een product is niet gelukt.")
      }
    )
  }

  public editProduct(product: Product): void {
    let data =
      {
        id: product.id,
        title: product.title,
        description: product.description,
        price: product.price,
        picture: product.picture
      };

    this.api.put<void>('/products', data).subscribe(

      data => {
        alert("Het aanpassen van een product is gelukt.");
      },
      error => {
        alert("Het aanpassen van een product is niet gelukt.")
      }
    )
  }

  public deleteProduct(product: Product): void {
    this.api.delete<void>('products/'+ product.id).subscribe(

      data => {
        alert("Het verwijderen van een product is gelukt.");
      },
      error => {
        alert("Het verwijderen van een product is niet gelukt.")
      }
    )
  }

  public getProductById(id: number) {
    return this.api.get<Product>('products/' + id)
  }
}
